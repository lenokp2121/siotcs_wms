package app;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;

import com.lowagie.text.*;
import com.lowagie.text.pdf.PdfWriter;

public class MainReport {

	public static void main(String[] args) {
		
		Document document = new Document();
		
		try {
			
			PdfWriter.getInstance(document, new FileOutputStream("F:\\Desenvolvimento_JAVAEE\\workspace32\\PDF_DevMedia.pdf"));
			document.setPageSize(PageSize.A4); // modificado para o tamanho A3;
			
			document.open();
			
			//Adicionando um Paregrafo
			document.add(new Paragraph("Gerando PDF - Java 1"));
			document.add(new Paragraph("Gerando PDF - Java 2"));
			document.add(new Paragraph("Gerando PDF - Java 3"));
			document.add(new Paragraph("Gerando PDF - Java 4"));
			
			Image figura = Image.getInstance("https://www.linuxvoice.com/wp-content/uploads/2016/12/adobe-pdf-icon.png");
			document.add(figura);
			
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (DocumentException e) {
			e.printStackTrace();
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			document.close();
		}

	}

}
