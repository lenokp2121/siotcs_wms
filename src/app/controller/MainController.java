package app.controller;

import java.io.IOException;

import app.StaticPanel;
import br.com.supremeforever.suprememdiwindow.MDICanvas;
import br.com.supremeforever.suprememdiwindow.MDIWindow;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.MenuItem;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class MainController extends Application {

	int i = 1;

	@FXML
	private MenuItem mniUsuario;

	public static MDICanvas canvas;

	@Override
	public void start(Stage primaryStage) throws Exception {

		canvas = new MDICanvas();
		canvas.setPrefSize(800, 800);

		// VBox box = new VBox(b, canvas);
		VBox box = (VBox) FXMLLoader.load(getClass().getResource("/app/view/Main.fxml"));
		box.getChildren().add(canvas);

		AnchorPane.setBottomAnchor(box, 0d);
		AnchorPane.setTopAnchor(box, 0d);
		AnchorPane.setLeftAnchor(box, 0d);
		AnchorPane.setRightAnchor(box, 0d);

		AnchorPane pane = new AnchorPane(box);
		Scene scene = new Scene(pane);

		primaryStage.setScene(scene);
		primaryStage.show();

	}

	public static void main(String[] args) {
		launch(args);
	}

	@FXML
	void novoUsuario(ActionEvent event) {

		Pane root;

		try {

			root = (Pane) FXMLLoader.load(getClass().getResource("/app/view/UsuarioView.fxml"));
			MDIWindow mdiWindow = new MDIWindow("myId" + i, new ImageView("http://icongal.com/gallery/image/129542/error_bug.png"), "My Title Windons" + i, root);
			mdiWindow.setPrefWidth(400);
			mdiWindow.setPrefHeight(400);
			canvas.addMDIWindow(mdiWindow);
			i++;

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
