package app.controller;

import app.model.Usuario;
import app.model.UsuarioAcesso;
import app.model.UsuarioAcessoDao;
import app.model.UsuarioDao;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public class UsuarioController {

    @FXML
    private TextField txfNome;

    @FXML
    private TextField txfSobrenome;

    @FXML
    private TextField txfUsuario;

    @FXML
    private PasswordField psfSenha;

    @FXML
    void limparCampos(ActionEvent event) {

    	txfNome.setText("");
    	txfSobrenome.setText("");
    	txfUsuario.setText("");
    	psfSenha.setText("");
    	
    }

    @FXML
    void salvarUsuario(ActionEvent event) {
    	
    	Usuario usuario = new Usuario();
    	usuario.setUNome(txfNome.getText());
    	usuario.setUSobrenome(txfSobrenome.getText());
    	
    	if(!txfUsuario.getText().isEmpty()) {
    		UsuarioDao.salvarUsuario(usuario);
    		
    		UsuarioAcesso usuarioAcesso = new UsuarioAcesso();
    		usuarioAcesso.setUaUsuario(txfUsuario.getText());
    		usuarioAcesso.setUaSenha(psfSenha.getText());
    		
    		UsuarioAcessoDao.salvarUsuario(usuarioAcesso);
    	}else {
    		UsuarioDao.salvarUsuario(usuario);
    	}
    	

    	limparCampos(event);
    }
    
    
    public static void iniciaRotina(Stage primaryStage) {
    	
    	try {
			Parent root = FXMLLoader.load(UsuarioController.class.getResource("/app/view/UsuarioView.fxml"));
			Scene scene = new Scene(root);
			scene.getStylesheets().add(UsuarioController.class.getResource("/app/resources/css/application.css").toExternalForm());
			primaryStage.setScene(scene);
			primaryStage.show();
		} catch(Exception e) {
			e.printStackTrace();
		}
    	
    }

}