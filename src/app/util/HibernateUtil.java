package app.util;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class HibernateUtil {

	private static final SessionFactory sessionFactory = buildSessionFactory();

	private static SessionFactory buildSessionFactory() {
		try {
			return new Configuration().configure().buildSessionFactory();
		} catch (Throwable tex) {
			System.out.println("Inicialização da SessionFactory Falhou!" + tex);
			throw new ExceptionInInitializerError(tex);
		}
	}
	
	public static SessionFactory getSessionFactory() {
		return sessionFactory;
	}
}