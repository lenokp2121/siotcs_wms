package app.model;

import org.hibernate.Session;

import app.util.HibernateUtil;

public class UsuarioAcessoDao {

	private static Session session;

	public static void salvarUsuario(UsuarioAcesso usuarioAcesso) {

		session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		session.save(usuarioAcesso);
		session.getTransaction().commit();
		session.close();
	}

}
