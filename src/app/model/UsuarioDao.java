package app.model;

import java.util.List;

import org.hibernate.Session;

import app.util.HibernateUtil;

public class UsuarioDao {
	
	private static Session session;
	
	public static void salvarUsuario(Usuario usuario) {
		
		session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		session.save(usuario);
		//session.createQuery("").getV;
		session.getTransaction().commit();
		session.close();
	}
	
	
	public static List<Usuario> listar(){
		
		List<Usuario> usuarios = null;
		
		session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		
		usuarios = session.createQuery("FROM Usuario ").list();
		
		session.getTransaction().commit();
		
		for(Usuario u : usuarios) {
			System.out.println(" Nome Usuario: " + u.getUNome());
		}
		
		
		return usuarios;
		
	}

}
