package app;
	
import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.fxml.FXMLLoader;



public class Main extends Application {
	@Override
	public void start(Stage primaryStage) {
		try {
			//BorderPane root = (BorderPane)FXMLLoader.load(getClass().getResource("/app/view/Main.fxml"));
			StaticPanel.parent = (BorderPane)FXMLLoader.load(getClass().getResource("/app/view/Main.fxml"));
			//Scene scene = new Scene(root);
			Scene scene = new Scene(StaticPanel.parent);
			scene.getStylesheets().add(getClass().getResource("/app/resources/css/application.css").toExternalForm());
			primaryStage.setScene(scene);
			primaryStage.setMaximized(true);
			primaryStage.show();
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		
	}
	
	public static void main(String[] args) {
		launch(args);
	}
}
