package app.report;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.List;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Paragraph;
import com.lowagie.text.pdf.PdfWriter;

import app.model.Usuario;
import app.model.UsuarioDao;

public class UsuarioReport {
	
	public static void relatorioDeUsuarios(String local, String nomeArquivo) throws DocumentException, FileNotFoundException {
		
		Document document = new Document();
		
		PdfWriter.getInstance(document, new FileOutputStream(local + nomeArquivo));
		
		document.open();
		
		List<Usuario> listUsuarios = UsuarioDao.listar();
		
		for(Usuario usuario : listUsuarios) {
			document.add(new Paragraph("ID: " + usuario.getUId() + " NOME: " + usuario.getUNome() + " SOBRENOME: " + usuario.getUSobrenome()));
		}
		
		document.close();
		
	}

}
